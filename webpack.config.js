const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const outputDir = './src/dist';
const entry = './src/js/main.js';
const cssOutput = 'bundle.css';

module.exports = {
    entry: entry,
    output: {
        path: path.join(__dirname, outputDir),
        filename: 'bundle.js',
        publicPath: '/dist/'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    use: ['css-loader'],
                    fallback: 'style-loader'
                })
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    use: ['css-loader', 'sass-loader'],
                    fallback: 'style-loader'
                })
            },
            {
                test: /\.(png|jpg|gif|woff2|woff)$/i,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 8,
                            publicPath: ( '.'),
                        },
                    },
                ],
            },
        ]
    },
    plugins: [
        new ExtractTextPlugin(cssOutput)
    ]
};